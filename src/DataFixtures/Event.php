<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class Event extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $em)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 100; $i++){
            $event= new \App\Entity\Event();
            $event->setDate($faker->dateTime);
            $event->setName($faker->word);
            $event->setDescription($faker->text);
            $event->setPrice($faker->randomFloat(2,1,100));
            $event->setLocation($faker->city);

            $em->persist($event);
        }

        $em->flush();
    }
}
